.. PFLOTRAN documentation master file, created by
   sphinx-quickstart on Thu Jul 14 07:37:39 2016.


**********************
PFLOTRAN QA Test Suite
**********************
.. image:: /_static/pflotran_logo.jpg

.. toctree::
   :maxdepth: 2
   
   /qa_tests/intro_howto.rst
   /qa_tests/intro_thermal.rst
   /qa_tests/intro_flow.rst
   /qa_tests/intro_gas.rst
   /qa_tests/intro_transport.rst

.. toctree::
   :hidden:
   
   include_toctree_thermal_steady.rst
   include_toctree_thermal_transient.rst
   include_toctree_flow_steady.rst
   include_toctree_flow_transient.rst
   include_toctree_gas_steady.rst
   include_toctree_transport_transient.rst

