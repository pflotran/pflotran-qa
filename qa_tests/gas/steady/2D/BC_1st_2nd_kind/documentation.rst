.. _gas-steady-2D-pressure-BC-1st-2nd-kind:

***********************************************
2D Steady Gas (Pressure), BCs of 1st & 2nd Kind
***********************************************
:ref:`gas-steady-2D-pressure-BC-1st-2nd-kind-description`

:ref:`gas-general-steady-2D-pressure-BC-1st-2nd-kind-pflotran-input`

:ref:`gas-steady-2D-pressure-BC-1st-2nd-kind-dataset`

:ref:`gas-steady-2D-pressure-BC-1st-2nd-kind-python`


.. _gas-steady-2D-pressure-BC-1st-2nd-kind-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.3.3, pg.42, "A 2D Steady-State 
Gas Pressure Distribution, Boundary Conditions of 1st and 2nd Kind."

The domain is a 1x1x0.5 meter rectangular plate extending along the positive 
x-axis and y-axis and is made up of 20x20x1 hexahedral grid cells with 
dimensions 0.05x0.05x0.5 meters. The materials are assigned the following 
properties: porosity = 0.5; gas viscosity :math:`\mu` = 1.0e-5 Pa-s; 
permeability = 1e-15 m^2; gas saturation :math:`S_g` = 1.0 in the entire domain.

The gas pressure is initially uniform at *p(t=0)* = 1.0e5 Pa.
At the west and south boundaries, a dirichlet gas pressure *p(x=0,y=(0,L))* = 
*p(x=(0,L),y=0)* = 1.0e5 Pa is applied, where L = 1 m. At the north and east
boundaries, a specific gas flow [Pa m/s] is applied that is a function of x 
and y,

.. math::

   Q = \frac{3 k p_0^2 y}{2 \mu L^2} \hspace{0.5 in} east \hspace{0.2 in} face
   
   Q = \frac{3 k p_0^2 x}{2 \mu L^2} \hspace{0.5 in} north \hspace{0.2 in} face

PFLOTRAN does not have the capability to handle such a boundary condition.
For an explanation on how a specific gas flow is treated, see 
:ref:`gas-steady-1D-pressure-BC-1st-2nd-kind`.

The simulation is run until the steady-state pressure distribution
develops, which is 20 years. 

The LaPlace equation governs the steady-state gas pressure distribution,

.. math:: 

   {{\partial^{2} p^{2}} \over {\partial x^{2}}} + {{\partial^{2} p^{2}} \over {\partial y^{2}}} = 0

When the boundary conditions are applied, the solution becomes,

.. math:: 

   p(x,y) = p_0 \sqrt{1 + 3\frac{xy}{L^2}}
   
where :math:`p_0` is the initial pressure, *p(t=0)* = 1.0e5 Pa.

.. figure:: ../qa_tests/gas/steady/2D/BC_1st_2nd_kind/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/gas/steady/2D/BC_1st_2nd_kind/general_mode/comparison_plot.png
   :width: 49 %  
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
   
   
.. _gas-general-steady-2D-pressure-BC-1st-2nd-kind-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The GENERAL Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/gas/steady/2D/BC_1st_2nd_kind/general_mode/2D_steady_gas_BC_1st_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/gas/steady/2D/BC_1st_2nd_kind/general_mode/2D_steady_gas_BC_1st_2nd_kind.in



.. _gas-steady-2D-pressure-BC-1st-2nd-kind-dataset:

The Dataset
===========
The hdf5 dataset required to define the boundary conditions is created
with the following python script called ``create_dataset.py``:

.. literalinclude:: ../qa_tests/gas/steady/2D/BC_1st_2nd_kind/create_dataset.py



.. _gas-steady-2D-pressure-BC-1st-2nd-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: gas_steady_2D_BC1st2ndkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
