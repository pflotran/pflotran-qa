import sys

nx = 1; dx = 1; lx = 1
ny = 1; dy = 1; ly = 1
nz = 1; dz = 1; lz = 1
filename = ''
for k in range(len(sys.argv)-1):
  k = k + 1
  if sys.argv[k] == '-nx':
    nx = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-ny':
    ny = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-nz':
    nz = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-dx':
    dx = float(sys.argv[k+1])
  if sys.argv[k] == '-dy':
    dy = float(sys.argv[k+1])
  if sys.argv[k] == '-dz':
    dz = float(sys.argv[k+1])
  if sys.argv[k] == '-lx':
    lx = float(sys.argv[k+1])
  if sys.argv[k] == '-ly':
    ly = float(sys.argv[k+1])
  if sys.argv[k] == '-lz':
    lz = float(sys.argv[k+1])
  if sys.argv[k] == '-input_prefix':
    filename = sys.argv[k+1] + '.in'
    
f = open(filename,'w')

simulation = """ 
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_TRANSPORT transport
      GLOBAL_IMPLICIT
    /
  /
END

"""
chemistry = """
CHEMISTRY
  PRIMARY_SPECIES
    tracer
  /
  #LOG_FORMULATION
  OUTPUT
    TOTAL
    ALL
  /
END
"""
linear_solver = """
LINEAR_SOLVER TRANSPORT
   SOLVER DIRECT
END
"""
grid = """
GRID
  TYPE structured
  NXYZ """ + str(nx) + " " + str(ny) + " " + str(nz) + """
  DXYZ
   """ + str(dx) + """d0
   """ + str(dy) + """d0
   """ + str(dz) + """d0
  END
END
"""

mid_low = lx/2.0 - dx/2.0
mid_high = lx/2.0 + dx/2.0

region = """
REGION all
  COORDINATES
    0.0d0 0.0d0 0.0d0
    """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION first_cell
  COORDINATES
    0.d0 0.d0 0.d0
    """ + str(int(dx)) + """.d0 1.d0 1.d0
  /
END

REGION middle_cell
  COORDINATES
    """ + str(mid_low)  + """d0 0.0d0 0.0d0
    """ + str(mid_high) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END

"""
mat_prop = """
MATERIAL_PROPERTY soil1
  ID 1
  POROSITY 1.d0
  TORTUOSITY 1.d0
END
"""
fluid_prop = """
FLUID_PROPERTY
  DIFFUSION_COEFFICIENT 1.d-9
END
"""
strata = """
STRATA
  REGION all
  MATERIAL soil1
END
"""
time = """
TIME
  FINAL_TIME 80.d0 y
  INITIAL_TIMESTEP_SIZE 1.d0 h
  MAXIMUM_TIMESTEP_SIZE 1.d-1 yr
END
"""
output = """
OUTPUT
  SNAPSHOT_FILE
    TIMES y 10. 20. 40. 80.
    FORMAT HDF5
  /
END
"""
flow_cond = """
TRANSPORT_CONDITION initial
  TYPE DIRICHLET
  CONSTRAINT_LIST
    0.d0 initial
  /
END

TRANSPORT_CONDITION initial_conc
  TYPE DIRICHLET
  CONSTRAINT_LIST
    0.d0 initial_conc
  /
END
"""
init_cond = """
INITIAL_CONDITION initial
  REGION all
  TRANSPORT_CONDITION initial
END

INITIAL_CONDITION intial_conc
  REGION middle_cell
  TRANSPORT_CONDITION initial_conc
END

"""
constraints = """
CONSTRAINT initial_conc
  CONCENTRATIONS 
    tracer 20.d0      T
  /
END

CONSTRAINT initial
  CONCENTRATIONS
    tracer 1.d-20     T
  /
END
"""
  
f.write(simulation)
f.write("SUBSURFACE\n")
f.write(chemistry)
f.write(linear_solver)
f.write(grid)
f.write(region)
f.write(mat_prop)
f.write(fluid_prop)
f.write(strata)
f.write(time)
f.write(output)
f.write(flow_cond)
f.write(init_cond)
f.write(constraints)
f.write("\nEND_SUBSURFACE\n") 
# Must have \n at the end, or HDF5 output will not work
