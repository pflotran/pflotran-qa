.. _flow-transient-1D-pressure-BC-1st-2nd-kind:

***************************************************
1D Transient Flow (Pressure), BCs of 1st & 2nd Kind
***************************************************
:ref:`flow-transient-1D-pressure-BC-1st-2nd-kind-description`

:ref:`flow-general-transient-1D-pressure-BC-1st-2nd-kind-pflotran-input`

:ref:`flow-th-transient-1D-pressure-BC-1st-2nd-kind-pflotran-input`

:ref:`flow-richards-transient-1D-pressure-BC-1st-2nd-kind-pflotran-input`

:ref:`flow-transient-1D-pressure-BC-1st-2nd-kind-dataset`

:ref:`flow-transient-1D-pressure-BC-1st-2nd-kind-python`



.. _flow-transient-1D-pressure-BC-1st-2nd-kind-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.2.9, pg.35, "A Transient 1D 
Pressure Distribution, Non-Zero Initial Pressure, Boundary Conditions of 
1st and 2nd Kind."

The domain is a 100x1x1 meter rectangular beam extending along the positive 
x-axis and is made up of 50x1x1 hexahedral grid cells with dimensions 2x1x1 
meters. The domain is composed of a single material and is assigned the 
following properties: porosity :math:`\phi` = 0.20; permeability :math:`k` = 
1.0e-14 m^2; rock density :math:`\rho` = 2,000 kg/m^3; fluid compressibility
:math:`K` = 1.0e-9 1/Pa; fluid viscosity :math:`\mu` = 1.728e-3 Pa-sec.

The pressure is initially distributed according to p(x,t=0)=f(x), where
f(x) is defined (in units of MPa) as

.. math:: 
   f(x) = 0  \hspace{0.25in} 0 \leq x < {L \over 10}
   
   f(x) = {{10x} \over {3L}}-{1 \over 3}  \hspace{0.25in} {L \over 10} \leq x < {{4L} \over 10}
   
   f(x) = 1  \hspace{0.25in} {{4L} \over 10} \leq x < {{6L} \over 10}
   
   f(x) = 3-{{10x} \over {3L}}  \hspace{0.25in} {{6L} \over 10} \leq x < {{9L} \over 10}
   
   f(x) = 0  \hspace{0.25in} {{9L} \over 10} \leq x \leq L

At the two boundaries, a no fluid flux condition is applied,

.. math::
   q(0,t) = 0
   
   q(L,t) = 0

where L = 100 m. The transient pressure distribution is governed by,

.. math:: 
   {\phi K} {{\partial p} \over {\partial t}} = {k \over \mu} {{\partial^{2} p} \over {\partial x^{2}}}

With the initial pressure given, the solution is defined by,

.. math:: 
   p(x,t) = {1 \over 2} + \sum_{n=1}^{\infty} exp\left({-\chi n^2 \pi^2 {t \over L^2}}\right)\left({80 \over {3(n\pi)^2}}\right) cos{{n \pi y} \over L} cos{{n\pi} \over 2} sin{{n\pi} \over 4} sin{{3n\pi} \over 20} 
  
  \chi = {{k} \over {\phi \mu K}}
   
.. figure:: ../qa_tests/flow/transient/1D/BC_1st_2nd_kind/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/flow/transient/1D/BC_1st_2nd_kind/general_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
.. figure:: ../qa_tests/flow/transient/1D/BC_1st_2nd_kind/th_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for TH mode.
   
.. figure:: ../qa_tests/flow/transient/1D/BC_1st_2nd_kind/richards_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for RICHARDS mode.


   
.. _flow-general-transient-1D-pressure-BC-1st-2nd-kind-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The GENERAL Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/transient/1D/BC_1st_2nd_kind/general_mode/1D_transient_pressure_BC_1st_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/flow/transient/1D/BC_1st_2nd_kind/general_mode/1D_transient_pressure_BC_1st_2nd_kind.in



.. _flow-th-transient-1D-pressure-BC-1st-2nd-kind-pflotran-input:

The PFLOTRAN Input File (TH Mode)
=================================
The TH Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/transient/1D/BC_1st_2nd_kind/th_mode/1D_transient_pressure_BC_1st_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/flow/transient/1D/BC_1st_2nd_kind/th_mode/1D_transient_pressure_BC_1st_2nd_kind.in



.. _flow-richards-transient-1D-pressure-BC-1st-2nd-kind-pflotran-input:

The PFLOTRAN Input File (RICHARDS Mode)
=======================================
The RICHARDS Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/transient/1D/BC_1st_2nd_kind/richards_mode/1D_transient_pressure_BC_1st_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/flow/transient/1D/BC_1st_2nd_kind/richards_mode/1D_transient_pressure_BC_1st_2nd_kind.in



.. _flow-transient-1D-pressure-BC-1st-2nd-kind-dataset:

The Dataset
===========
The hdf5 dataset required to define the initial/boundary conditions is created
with the following python script called ``create_dataset.py``:

.. literalinclude:: ../qa_tests/flow/transient/1D/BC_1st_2nd_kind/create_dataset.py
 
 

.. _flow-transient-1D-pressure-BC-1st-2nd-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: flow_transient_1D_BC1st2ndkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.