INSTRUCTIONS ON HOW TO RUN THE QA TESTS:

In the repo-name/qa_tests directory, run the "run_qa_tests.py" script
by typing in the following command:

> python run_qa_tests.py -E=/path/to/PFLOTRAN/executable -ALL

The argument -E and the path that follows indicates your PFLOTRAN
executable which will run the tests. The argument -ALL will run all
tests. There are other run options. To see all options, add the argument -HELP.

Running the tests will generate screen output, as well as a report card
called report.txt in the repo-name/qa_tests directory. It will also
generate several figures for each test, which are located in their
respective test directories within repo-name/qa_tests.

Note: You must run the QA tests at least once to generate the figures
required for the documentation before you generate the documentation
in repo-name/documentation, otherwise you will get errors on the
documentation build saying that figures are missing.
