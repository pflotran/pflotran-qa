.. _thermal-transient-1D-conduction-BC-2nd-kind:

************************************************
1D Transient Thermal Conduction, BCs of 2nd Kind 
************************************************
:ref:`thermal-transient-1D-conduction-BC-2nd-kind-description`

:ref:`thermal-general-transient-1D-conduction-BC-2nd-kind-pflotran-input`

:ref:`thermal-th-transient-1D-conduction-BC-2nd-kind-pflotran-input`

:ref:`thermal-transient-1D-conduction-BC-2nd-kind-python`



.. _thermal-transient-1D-conduction-BC-2nd-kind-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.1.7, pg.20, "A Transient 1D 
Temperature Distribution, Time-Dependent Boundary Conditions of 2nd Kind."

The domain is a 25x1x1 meter rectangular column extending along the positive 
x-axis and is made up of 50x1x1 cubic grid cells with dimensions 
0.5x1x1 meters. The domain material is assigned the following properties: 
thermal conductivity *K* = 1.16 W/(m-C); specific heat capacity *Cp* = 0.01 
J/(m-C); density :math:`\rho` = 2,000 kg/m^3. 
**The accuracy of the numerical solution**
**is very sensitive to the time step size because the heat flux value is held**
**constant at the value at the beginning of the timestep. Reducing the timestep**
**will make the numerical solution more accurate relative to the analytical**
**solution, but note this is not an issue with time truncation error.**

The temperature is initially uniform at *T(t=0)* = 0 C.
The heat flux boundary conditions (in units of W/m2) are:

.. math::
   q(0,t) = 0
   
   q(L,t) = 0.385802 t

where L = 25 m. The transient temperature distribution is governed by,

.. math:: 
   \rho c_p {{\partial T} \over {\partial t}} = K {{\partial^{2} T} \over {\partial x^{2}}}

With the given boundary conditions, the solution is defined by,

.. math::
   T(x,t) = {{8q\sqrt{\chi t^3}} \over K} \sum_{n=0}^{\infty} \left[{ i^3erfc{{(2n+1)L-x}\over{2\sqrt{\chi t}}} + i^3erfc{{(2n+1)L+x}\over{2\sqrt{\chi t}}} }\right]
   
where :math:`i^3erfc(g)` represents the third repeated integral of the
complimentary error function, given by,

.. math::
   i^3erfc(g) = {2 \over \pi} \int^{\infty}_{g} {{(s-g)^3}\over{3!}} e^{-s^2} ds

.. figure:: ../qa_tests/thermal/transient/1D/BC_2nd_kind/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/thermal/transient/1D/BC_2nd_kind/general_mode/comparison_plot.png
   :width: 49 %   
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
.. figure:: ../qa_tests/thermal/transient/1D/BC_2nd_kind/th_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for TH mode.
   
   
   
.. _thermal-general-transient-1D-conduction-BC-2nd-kind-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The General Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/thermal/transient/1D/BC_2nd_kind/general_mode/1D_transient_thermal_BC_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/thermal/transient/1D/BC_2nd_kind/general_mode/1D_transient_thermal_BC_2nd_kind.in



.. _thermal-th-transient-1D-conduction-BC-2nd-kind-pflotran-input:

The PFLOTRAN Input File (TH Mode)
=================================
The TH Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/thermal/transient/1D/BC_2nd_kind/th_mode/1D_transient_thermal_BC_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/thermal/transient/1D/BC_2nd_kind/th_mode/1D_transient_thermal_BC_2nd_kind.in


  
.. _thermal-transient-1D-conduction-BC-2nd-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: thermal_transient_1D_BC2ndkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
