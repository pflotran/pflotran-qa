.. _thermal-steady-2D-conduction-BC-1st-kind:

*********************************************
2D Steady Thermal Conduction, BCs of 1st Kind 
*********************************************
:ref:`thermal-steady-2D-conduction-BC-1st-kind-description`

:ref:`thermal-general-steady-2D-conduction-BC-1st-kind-pflotran-input`

:ref:`thermal-th-steady-2D-conduction-BC-1st-kind-pflotran-input`

:ref:`thermal-steady-2D-conduction-BC-1st-kind-dataset`

:ref:`thermal-steady-2D-conduction-BC-1st-kind-python`



.. _thermal-steady-2D-conduction-BC-1st-kind-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.1.3, pg.16, "A 2D Steady-State 
Temperature Distribution, Boundary Conditions of 1st Kind."

The domain is a 1x1x1 meter slab extending along the positive 
x-axis and y-axis and is made up of 10x10x1 hexahedral grid cells with 
dimensions 0.1x0.1x1 meters. The domain material is assigned the following 
properties: thermal conductivity *K* = 1.0 W/(m-C); specific heat capacity 
*Cp* = 0.001 J/(m-C); density *rho* = 2,800 kg/m^3.

The temperature is initially uniform at *T(t=0)* = 1.0 C.
The boundary temperatures are:

.. math::
  T(0,y) = 0 \hspace{0.25in} x=0 \hspace{0.15in} face
  
  T(L,y) = T0 {y \over L}        \hspace{0.25in} x=L \hspace{0.15in} face
  
  T(x,0) = 0 \hspace{0.25in} y=0 \hspace{0.15in} face
  
  T(x,L) = T0 {x \over L}        \hspace{0.25in} y=L \hspace{0.15in} face

where L = 1 m and T0 = 1.0 C.
The simulation is run until the steady-state temperature distribution
develops. 

The LaPlace equation governs the steady-state temperature distribution,

.. math:: {{\partial^{2} T} \over {\partial x^{2}}} + {{\partial^{2} T} \over {\partial y^{2}}}= 0

The solution is given by,

.. math:: T(x,y) = T_{t=0} {x \over L}{y \over L}

.. figure:: ../qa_tests/thermal/steady/2D/BC_1st_kind/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/thermal/steady/2D/BC_1st_kind/general_mode/comparison_plot.png
   :width: 49 %   
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
.. figure:: ../qa_tests/thermal/steady/2D/BC_1st_kind/th_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for TH mode.
   
   
   
.. _thermal-general-steady-2D-conduction-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The General Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/thermal/steady/2D/BC_1st_kind/general_mode/2D_steady_thermal_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/thermal/steady/2D/BC_1st_kind/general_mode/2D_steady_thermal_BC_1st_kind.in



.. _thermal-th-steady-2D-conduction-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (TH Mode)
=================================
The TH Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/thermal/steady/2D/BC_1st_kind/th_mode/2D_steady_thermal_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/thermal/steady/2D/BC_1st_kind/th_mode/2D_steady_thermal_BC_1st_kind.in


  
.. _thermal-steady-2D-conduction-BC-1st-kind-dataset:

The Dataset
===========
The hdf5 dataset required to define the initial/boundary conditions is created
with the following python script called ``create_dataset.py``:

.. literalinclude:: ../qa_tests/thermal/steady/2D/BC_1st_kind/create_dataset.py
  
  
  
.. _thermal-steady-2D-conduction-BC-1st-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: thermal_steady_2D_BC1stkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
