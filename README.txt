INFORMATION AND INSTRUCTIONS:

This repository consists of QA testing suite.

Inside repo-name/documentation/ you will find the documentation 
file structure for generating documentation with Sphinx. It consists
or an organized, hierarchical set of restructured text files, figures,
and a _build directory which is created with Sphinx and contains the 
html and pdf versions of the documentation.

Inside the repo-name/qa_tests/ you will find the file structure for
the verification testing suite. It consists of an organized, hierarchical
set of input files, python scripts, and figures.

STEP 1: Run the QA Test Suite inside the repo-name/qa_tests/ directory.
Instructions are given in repo-name/qa_tests/README.txt. This step only
has to be done once, or every time you want an updated set of QA test
results to be included in the documentation.

STEP 2: Generate the documentation using Sphinx inside the 
repo-name/documentation/ directory. Instructions are given in the
repo-name/documentation/README.txt file.


