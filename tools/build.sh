#!/bin/sh

# environment variables
MAKE_LOG="make.log"
export PETSC_MASTER=0
export CMAKE_BUILD=0
# the following conditional allows one to run this script outside of
# codeship
if [ -d /app ]; then
  APP_DIR=/app
else
  APP_DIR=`pwd`
fi

echo 'begin build'

echo 'building pflotran'

cd $APP_DIR
git clone https://bitbucket.org/pflotran/pflotran.git
cd pflotran
echo 'building petsc'
./.travis/install-petsc.sh
echo 'compiling pflotran'
./.travis/pflotran-build.sh
if [ ! -e "$APP_DIR/pflotran/src/pflotran/pflotran" ]; then
  echo 'pflotran compilation failed'
  exit 1
fi
echo 'testing pflotran'
./.travis/pflotran-test.sh
# have to set exit status prior to call to 'cat' as it will reset exit status
test_exit_status=$?
cat regression_tests/*.testlog
if [ $test_exit_status -ne 0 ]; then
  echo 'pflotran unit and/or regression testing failed'
  exit 1
fi

echo 'running qa tests'
cd $APP_DIR/qa_tests
# petsc-arch is the PETSC_ARCH specified in the 
# $PFLOTRAN_DIR/.travis/install-petsc.sh file
python run_qa_tests.py -E=$APP_DIR/pflotran/src/pflotran/pflotran -MPI=$APP_DIR/pflotran/petsc/petsc-arch/bin/mpirun -ALL

echo 'building qa documentation'
cd $APP_DIR/documentation
echo 'building html'
make clean
make html 2>&1 | tee $MAKE_LOG
NUM_ERROR=$(grep -c "ERROR:" "$MAKE_LOG")
NUM_WARNING=$(grep -c "WARNING:" "$MAKE_LOG")
NUM_TOTAL=`expr $NUM_ERROR + $NUM_WARNING`
exit_status=0
if [ $NUM_TOTAL -eq 0 ]; then
  echo 'build succeeded'
  echo 'building tarball'
  cd _build/html
  tar -czvf /tmp/codeship.tar.gz *
  exit_status=$?
  if [ $exit_status -gt 0 ]; then
    echo 'tarball failed'
  else
    echo 'tarball succeeded'
  fi
else
  echo
  echo 'build failed due to the following warnings or errors'
  if [ $NUM_ERROR -gt 0 ]; then
    echo
    echo 'Errors:'
    echo
    grep 'ERROR:' $MAKE_LOG
  fi
  if [ $NUM_WARNING -gt 0 ]; then
    echo
    echo 'Warnings:'
    echo
    grep 'WARNING:' $MAKE_LOG
  fi
  echo
  echo 'build failed due to the preceding warnings or errors'
  echo
  exit_status=1
fi
exit $exit_status
